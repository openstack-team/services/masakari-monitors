#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

override_dh_auto_clean:
	rm -f debian/*.init debian/*.service debian/*.upstart
	rm -rf build
	rm -rf masakari.sqlite *.egg-info
	rm -f debian/masakari-monitors-common.postinst debian/masakari-common.postrm debian/masakari-monitors-common.postrm
	find . -type d -name __pycache__ -exec rm -r {} \+
	rm -f doc/source/_static/masakarimonitors.conf.sample

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func masakari-monitors-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm masakari-monitors-common.postrm

override_dh_auto_test:
	echo "Do nothing..."

override_dh_auto_install:
	echo "Do nothing..."

override_dh_install:
	for i in $(PYTHON3S) ; do \
		python$$i setup.py install --root=debian/tmp --install-layout=deb ; \
	done

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 'masakarimonitors\.tests\.unit.*'
endif

	# Generate the masakari.conf config
	mkdir -p $(CURDIR)/debian/masakari-monitors-common/usr/share/masakari-monitors-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/masakari-monitors-common/usr/share/masakari-monitors-common/masakarimonitors.conf \
		--wrap-width 140 \
		--namespace masakarimonitors.conf \
		--namespace oslo.log \
		--namespace oslo.middleware

	mkdir -p $(CURDIR)/debian/masakari-monitors-common/etc/masakarimonitors
	cp $(CURDIR)/etc/masakarimonitors/process_list.yaml.sample $(CURDIR)/debian/masakari-monitors-common/etc/masakarimonitors/process_list.yaml

#	mkdir -p $(CURDIR)/debian/masakari-process-monitor/etc/masakarimonitors
#	cp $(CURDIR)/etc/masakarimonitors/processmonitor.conf.sample $(CURDIR)/debian/masakari-process-monitor/etc/masakarimonitors/processmonitor.conf

	dh_install
	rm -rf $(CURDIR)/debian/tmp/usr/etc
	dh_missing --fail-missing

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. PYTHON=python3 python3 -m sphinx -b html doc/source $(CURDIR)/debian/masakari-monitors-doc/usr/share/doc/masakari-monitors-doc/html
	dh_sphinxdoc
endif

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
